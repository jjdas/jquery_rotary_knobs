$(function ()
{
    //ATTRIBUTES
    //Id
    //Info
    var Size = 100;
    var Min = 0;
    var Max = 127;
    var Value = 64;
    var Thickness = 12;
    var Padding = 10;
    var Gap = 5;
    var Rotate = 30;
    var Backcolor = "#eceeef";
    var Frontcolor = "#87ceeb";
    var Fontcolor = "#87ceeb";
    var Fontsize = "20";
    var Floats = "false";
    var Decimals = 2;
    var Indicator = "false";
    var Indicatorwidth = 1;
    var Indicatorcolor = "black";
    var Readonly = "false";
    var Hidelabel = "false";
    var Nomanual = "false";
    var Font = "'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif";

    //NOT CONFIGURABLE THROUGH HTML    
    var InputWidth = "40";
    var InputHeight = "24";
    var detailedErrors = false;

    //GLOBAL VARIABLES
    var mouseDown = false;
    var scrollY = $(window).scrollTop();
    var scrollX = $(window).scrollLeft();
    var undo;
    
    var clickedObject;
    var inputVisible = false;

    //INITIALIZE
    var index = 0;

    $('.rotary').each(function ()
    {
        index++;
        var id = $(this).data("id"); if (id == null) id = "rotary" + index;
        var info = $(this).data("info");
        var size = $(this).data("size"); if (isNaN(size)) size = Size;
        var min = $(this).data("min"); if (isNaN(min)) min = Min;
        var max = $(this).data("max"); if (isNaN(max)) max = Max;
        var value = $(this).data("value"); if (isNaN(value)) value = Value;
        var gap = $(this).data("gap"); if (isNaN(gap)) gap = Gap;
        var rotate = $(this).data("rotate"); if (isNaN(rotate)) rotate = Rotate;
        var thickness = $(this).data("thickness"); if (isNaN(thickness)) thickness = Thickness;
        var padding = $(this).data("padding"); if (isNaN(padding)) padding = Padding;
        var frontcolor = $(this).data("frontcolor"); if (frontcolor == null) frontcolor = Frontcolor;
        var backcolor = $(this).data("backcolor"); if (backcolor == null) backcolor = Backcolor;
        var fontcolor = $(this).data("fontcolor"); if (fontcolor == null) fontcolor = Fontcolor;
        var fontsize = $(this).data("fontsize"); if (isNaN(fontsize)) fontsize = Fontsize;
        var floats = ($(this).data("floats")); if (floats == null) floats = Floats;
        var decimals = $(this).data("decimals"); if (isNaN(decimals)) decimals = Decimals;
        var indicator = $(this).data("indicator"); if (isNaN(indicator)) indicator = Indicator;
        var indicatorwidth = $(this).data("indicatorwidth"); if (isNaN(indicatorwidth)) indicatorwidth = Indicatorwidth;
        var indicatorcolor = $(this).data("indicatorcolor"); if (indicatorcolor == null) indicatorcolor = Indicatorcolor;
        var readonly = ($(this).data("readonly")); if (readonly == null) readonly = Readonly;
        var hidelabel = ($(this).data("hidelabel")); if (hidelabel == null) hidelabel = Hidelabel;
        var nomanual = ($(this).data("nomanual")); if (nomanual == null) nomanual = Nomanual;
        var font = ($(this).data("font")); if (font == null) font = Font;

        //FOR MANUAL INPUT POP UP
        var marginLeft = (size / 2) - (InputWidth / 2);
        var marginTop = (size / 2) - (InputHeight / 2);

        var html = '\
                <input class="manualInput" style="overflow:hidden;text-align: center; border:none; font-size:' + fontsize + 'px; font-family:\''+font+'\'; position:absolute;margin-left:' + marginLeft + 'px;margin-top:' + marginTop + 'px;width:' + InputWidth + 'px;height:' + InputHeight + 'px;display:none;"/>\
                <canvas class="rotaryCanvas"\
                style="user-select:none;"\
                id ="' + id + '"\
                data-info ="' + info + '"\
                width="' + size + '"\
                height="' + size + '"\
                data-min="' + min + '"\
                data-max="' + max + '"\
                data-value="' + value + '"\
                data-gap="' + gap + '"\
                data-rotate="' + rotate + '"\
                data-thickness="' + thickness + '"\
                data-padding="' + padding + '"\
                data-frontcolor="' + frontcolor + '"\
                data-backcolor="' + backcolor + '"\
                data-fontcolor="' + fontcolor + '"\
                data-fontsize="' + fontsize + '"\
                data-floats="' + floats + '"\
                data-decimals="' + decimals + '"\
                data-indicator="' + indicator + '"\
                data-indicator="' + indicator + '"\
                data-indicatorwidth="' + indicatorwidth + '"\
                data-indicatorcolor="' + indicatorcolor + '"\
                data-readonly="' + readonly + '"\
                data-hidelabel="' + hidelabel + '"\
                data-nomanual="' + nomanual + '"\
                data-font="' + font + '"\
            ></canvas>\
        ';
        $(this).append(html);
        $(this).data("value", value);
    });

    $('.rotaryCanvas').each(function () { draw($(this)) });


    //MOUSE UP
    $(document).mouseup(function ()
    {
        mouseDown = false;
        if(clickedObject != null)
        undo = clickedObject.data("value");
    });


    //WINDOW SCROLLING
    $(window).scroll(function (event)
    {
        scrollY = $(window).scrollTop();
        scrollX = $(window).scrollLeft();
    });


    //MOUSE DOWN
    $(".rotaryCanvas").mousedown(function (event)
    {
        mouseDown = true;
        undo = $(this).data("value");
        clickedObject = $(this);
        updateValues(event);
    });


    // MOUSE MOVE
    $(".rotaryCanvas").mousemove(function (event)
    {
        if (mouseDown && this == clickedObject[0]) { updateValues(event); }
    });


    //HIDE MANUAL INPUT FIELD AND COMMIT VALUE ON CLICK OUTSIDE
    $(document).click(function (event)
    {
        if (!$(event.target).hasClass("manualInput"))
        {
            commitManualInput();
        }
    });


    // ESCAPE and ENTER
    $(document).keyup(function (e)
    {
        if (mouseDown && e.keyCode === 27)
        {
            clickedObject.data("value", undo);
            draw(clickedObject);
        }

        if (e.keyCode === 13)
        {
            commitManualInput();
        }
    });


    //DOUBLE CLICK
    $(".rotaryCanvas").dblclick(function (event)
    {
        if (!$(this).data("nomanual"))
        {
            $('.manualInput').fadeOut('fast');
            var val = $(this).data("value");
            $(this).prev().val(val);
            $(this).prev().fadeIn('fast');
            $(this).prev().focus();
            inputVisible = true;
        }
    });


    function commitManualInput()
    {
        $(".manualInput").each(function ()
        {
            if ($(this).is(":visible"))
            {
                inputVisible = false;
                var v = $(this).val();
                var obj = $(this).next();

                var min = obj.data("min");
                var max = obj.data("max");

                v = clamp(v, min, max);

                obj.data("value", v);
                var id = obj.attr("id");
                var info = obj.data("info");

                toReceiver(id, v, info);
                draw(obj);
            }
        });

        $('.manualInput').fadeOut('fast');
    }


    function updateValues(event)
    {
        var id = clickedObject.attr("id");
        var info = clickedObject.data("info");
        var min = clickedObject.data("min");
        var max = clickedObject.data("max") + 1;
        var val = clickedObject.data("value");
        var gap = clickedObject.data("gap");
        var rotate = clickedObject.data("rotate");
        var rect = clickedObject[0].getBoundingClientRect();
        var floats = clickedObject.data("floats");
        var decimals = clickedObject.data("decimals");
        var readonly = clickedObject.data("readonly");  
        var hidelabel = clickedObject.data("hidelabel");  

        if (!readonly && !inputVisible)
        {
            var v = getValue(val, gap, rotate, min, max, rect, hidelabel, event);
           
            if (floats) v = v.toFixed(decimals);
            else v = parseInt(v);

            if (!isNaN(v))
            {
                clickedObject.data("value", v);
                draw(clickedObject);
                toReceiver(id, v, info);
            }
        }
    }


    function toReceiver(id, v, info)
    {
        try { knobReceiver(id, v, info); }
        catch (e)
        {
            console.log("window.knobReceiver = function(id, value, info){} is not present, unreachable or produces an error.");
            if (detailedErrors) console.log(e);
        }
    }


    //GET VALUE
    function getValue(val, gap, rotate, min, max, rect, hidelabel, event)
    {
        var w = (rect.width);
        var h = (rect.height);

        clickX = event.pageX - rect.left;
        clickY = event.pageY - rect.top;

        var dx = clickX - (w / 2) - scrollX;
        var dy = clickY - (h / 2) - scrollY;

        if (dy == 0) dy = 0.01;
        if (dx == 0) dx = 0.01;

        var dist = Math.sqrt((dx * dx) + (dy * dy));

        var sin = dx / dist;
        var radians = Math.asin(sin);
        var degrees = radians * (180 / Math.PI);


        var mindDist = InputHeight / 2;
        if (hidelabel) mindDist = 0;

        if (dist > mindDist)
        {
            if (dy >= 0 && dx >= 0) { }
            if (dy >= 0 && dx < 0) { degrees = 360 + degrees; }
            if (dy < 0 && dx < 0) { degrees = 180 - degrees; }
            if (dy < 0 && dx > 0) { degrees = 180 - degrees; }

            degrees = (degrees + (180 - (3 * gap)) + (6 * rotate)) % 360;
            dMax = (60 - gap) * 6;

            if (degrees > 0 && degrees < dMax)
            {
                var range = max - min;
                var v = range - ((degrees / ((60 - gap) * 6)) * range) + min;
                v = clamp(v, min, max);
                return v;
            }
        }
    }


    function draw(obj)
    {
        //OBJECT VARIABLES
        var val = obj.data("value");
        var min = obj.data("min");
        var max = obj.data("max");
        var gap = obj.data("gap");
        var rotate = obj.data("rotate");
        var thickness = obj.data("thickness");
        var padding = obj.data("padding");
        var backcolor = obj.data("backcolor");
        var frontcolor = obj.data("frontcolor");
        var fontcolor = obj.data("fontcolor");
        var fontsize = obj.data("fontsize");
        var floats = obj.data("floats");
        var decimals = obj.data("decimals");
        var indicator = obj.data("indicator");
        var indicatorwidth = obj.data("indicatorwidth");
        var indicatorcolor = obj.data("indicatorcolor");
        var hidelabel = obj.data("hidelabel");
        var font = obj.data("font");

        // CALCULATED VARIABLES
        var ctx = obj[0].getContext("2d");
        var w = (obj[0].width);
        var h = (obj[0].height);
        var r1 = (w / 2) - padding;
        var r2 = r1 - thickness;
        var offset = (2 / 60) * (rotate + (gap / 2) - 15) * Math.PI;
        var start = offset;
        var end = ((2 / 60) * (60 - gap) * Math.PI) + offset;
        var valueStep = ((2 * Math.PI) * ((60 - gap) / 60)) / (max - min);

        //START DRAWING
        ctx.clearRect(0, 0, w, h);

        //BACK
        ctx.beginPath();
        ctx.arc(w / 2, h / 2, r1, start, end);
        ctx.arc(w / 2, h / 2, r2, end, start, true);

        ctx.fillStyle = backcolor;
        ctx.fill();

        //FRONT
        ctx.beginPath();
        var v = (val - min) * valueStep;
        var current = v + offset;
        ctx.arc(w / 2, h / 2, r1, start, current);
        ctx.arc(w / 2, h / 2, r2, current, start, true);

        ctx.fillStyle = frontcolor;
        ctx.fill();

        //INDICATOR
        if (indicator)
        {
            var a = v + offset;
            var x1 = (Math.cos(a) * r1) + w / 2;
            var y1 = (Math.sin(a) * r1) + h / 2;
            var x2 = (Math.cos(a) * r2) + w / 2;
            var y2 = (Math.sin(a) * r2) + h / 2;

            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.lineTo(x2, y2);
            ctx.lineWidth = indicatorwidth;
            ctx.strokeStyle = indicatorcolor;
            ctx.stroke();
        }

        //TEXT
        if (!hidelabel)
        {
            ctx.font = fontsize + "px " + font; ctx.textAlign = "center"; ctx.textBaseline = "middle";
            ctx.fillStyle = fontcolor;

            if (floats) ctx.fillText(parseFloat(val).toFixed(decimals), w / 2, h / 2);
            else ctx.fillText(parseInt(val), w / 2, h / 2);
        }
        currentVal = val;
    }


    function clamp(num, min, max)
    {
        return num <= min ? min : num >= max ? max : num;
    }

});


