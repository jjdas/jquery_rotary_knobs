Usage

HTML
Elements with class rotary are converted to rotary knobs. Styling and values can be configured using data- attributes. This script needs jquery to run.

<head> 
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script> 
  	<script src="rotary-knobs.js"></script> 
  	<script src="script.js"></script> 
</head> 

<body> 
  <div class="rotary"></div> 
  <div class="rotary" data-frontcolor="red"> 
</body>

script.js
Each knobs has a unique id which can also be set manually. When a knob is turned it's id, current value and data-info are sent to the receiver:

window.knobReceiver = function(id, value, info) 
{ 
    console.log("Object id: " + id + " value: " + value + " info:" + info); 
}


Misc.

Cancel
Press Esc while updating a value to cancel and restore the last set value.

Doubleclick
If the value text is showing, doubleclick to set a knob's value using the keyboard.


Configuration options

Data attributes
To use data- attributes, add them to an element with a rotary class. For example, the third knob of the samples at the top of this page is configured like this:
<div class="rotary" style="border:1px solid #eceeef" data-nomanual="true" data-hidelabel="true" data-indicator="true" data-gap="0" data-padding="15" data-size="70" data-rotate="0" data-value="0" data-frontcolor="#eceeef" data-thickness="20" data-info="This knob has been customized using the data- attributes"></div> 
Omitted attributes revert to their default value. Note that styling can be applied to rotary elements as well.

The list below describes the available data- attributes.

data-size	
Width and height in pixels	
100

data-min	
Lowest value (minumum)	
0

data-max	
Highest value (maximum)	
127

data-value	
Default value	
64

data-thickness	
The width of the 'donut' in pixels. For a 'pie' (donut without a hole) calculate the thickness as (size - (2 * padding)) / 2.	
12

data-padding
Space between the object border and the knob (px).	
10

data-rotate	
Rotation in minutes. The default value (30) is the six o'clock position.	
30

data-gap	
Opening between min and max value in minutes. Set to 30 for a half circle.	
5

data-backcolor
Background shape color.	
#eceeef

data-frontcolor	
Foreground shape color.	
#87ceeb

data-fontcolor	
Value label text color.	
#87ceeb

data-font	
Font of the value label	
'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif

data-floats	
Set to true to output decimal numbers (floats) instead of whole numbers (integers)	
false

data-decimals	
Number of decimals to output if data-floats="true"	
2

data-indicator	
Show a line at the current value	
false

data-indicatorwidth	
Indicator line width in px
1

data-indicatorcolor	
Indicator line color (html color)	
black

data-readonly
Ignore mouse input	
false

data-hidelabel	
Don't show the value label	
false

data-nomanual	
Ignore doubleclick to set value by keyboard input
false